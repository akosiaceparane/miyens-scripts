function ms(selector){
	self = {};
	self.selector = selector;
	console.log(self.selector);
	self.element = document.querySelector(self.selector);

	this.googleFormSubmit = function(callback){
		self.element.onsubmit(callback);
		return self.element;
	}

	return self.element;
}